package com.tedlm.samplemvc.controller;

import com.tedlm.samplemvc.Repository.*;
import com.tedlm.samplemvc.model.*;
import com.tedlm.samplemvc.model.LexuryCar.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class Controllerx {

    private final ItemRepository itemRepository;
    private LexuryCarRepository lexuryCarRepository;
    private LexuryCloseRepository lexuryCloseRepository;
    private LexuryShoesRepository lexuryShoesRepository;
    private LexuryWatchesRepository lexuryWatchesRepository;

    @Autowired
    public Controllerx(ItemRepository itemRepository, LexuryCarRepository lexuryCarRepository,
                       LexuryCloseRepository lexuryCloseRepository,LexuryShoesRepository lexuryShoesRepository,
                       LexuryWatchesRepository lexuryWatchesRepository) {
        this.itemRepository = itemRepository;
        this.lexuryCloseRepository = lexuryCloseRepository;
        this.lexuryShoesRepository=lexuryShoesRepository;
        this.lexuryWatchesRepository= (LexuryWatchesRepository) lexuryWatchesRepository;
        this.lexuryCarRepository = lexuryCarRepository;
    }

    @ModelAttribute(name="buylexury")
    public Item myLexury(){
        return new Item();
    }

    @GetMapping
    public String getIndex(Model model){

        //car
        List<LexuryCar> lexuryCarItem = new ArrayList<>();
        lexuryCarRepository.findAll().forEach(i-> lexuryCarItem.add(i));
        model.addAttribute("car", lexuryCarItem);

        // close
        List<LexuryClose> x = new ArrayList<>();
        lexuryCloseRepository.findAll().forEach(j-> x.add(j));
        model.addAttribute("close", x);

        List<LexuryShoes> y = new ArrayList<>();
        lexuryShoesRepository.findAll().forEach(j-> y.add(j));
        model.addAttribute("shoe",y);

        List<LexuryWatches> z = new ArrayList<>();
        lexuryWatchesRepository.findAll().forEach(i->z.add(i));
        model.addAttribute("watch",z);


        return "index";
    }
    //@Valid Item item, Errors errors,@ModelAttribute ViewL viewL
    @PostMapping
    public String processDesign() {
        //Item saved = itemRepository.save(item);
        //order.addDesign(saved);

        return "redirect:/second";
    }


}
