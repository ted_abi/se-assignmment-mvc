package com.tedlm.samplemvc.Repository;

import com.tedlm.samplemvc.model.Item;
import org.springframework.data.repository.CrudRepository;

public interface ItemRepository extends CrudRepository<Item,Long> {
}
