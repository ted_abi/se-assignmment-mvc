package com.tedlm.samplemvc.Repository;

import com.tedlm.samplemvc.model.LexuryCar;
import com.tedlm.samplemvc.model.LexuryClose;
import org.springframework.data.repository.CrudRepository;

public interface LexuryCloseRepository  extends CrudRepository<LexuryClose,String> {
}
