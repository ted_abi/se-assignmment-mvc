package com.tedlm.samplemvc;

import com.tedlm.samplemvc.Repository.LexuryCarRepository;
import com.tedlm.samplemvc.Repository.LexuryCloseRepository;
import com.tedlm.samplemvc.Repository.LexuryShoesRepository;
import com.tedlm.samplemvc.Repository.LexuryWatchesRepository;
import com.tedlm.samplemvc.model.LexuryCar;
import com.tedlm.samplemvc.model.LexuryClose;
import com.tedlm.samplemvc.model.LexuryShoes;
import com.tedlm.samplemvc.model.LexuryWatches;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SamplemvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SamplemvcApplication.class, args);
	}

	@Bean
    CommandLineRunner runner(LexuryCarRepository lexuryCarRepository,
                             LexuryWatchesRepository lexuryWatchesRepository,
                             LexuryShoesRepository lexuryShoesRepository,
                             LexuryCloseRepository lexuryCloseRepository){
	    return args -> {
            lexuryCarRepository.save(new LexuryCar("cr1","Bugati", LexuryCar.Type.CARS));
            lexuryCarRepository.save(new LexuryCar("cr2","BMW", LexuryCar.Type.CARS));
            lexuryCarRepository.save(new LexuryCar("cr3","Ferari", LexuryCar.Type.CARS));
            lexuryWatchesRepository.save(new LexuryWatches("w1","Rolex",LexuryWatches.Type.WATCH));
            lexuryWatchesRepository.save(new LexuryWatches("w2","Patek Philippe",LexuryWatches.Type.WATCH));
            lexuryWatchesRepository.save(new LexuryWatches("w3","Blancpain",LexuryWatches.Type.WATCH));
            lexuryCloseRepository.save(new LexuryClose("c1","Puma",LexuryClose.Type.CLOSE));
            lexuryCloseRepository.save(new LexuryClose("c2","Suprime",LexuryClose.Type.CLOSE));
            lexuryCloseRepository.save(new LexuryClose("c3","Puma2",LexuryClose.Type.CLOSE));
            lexuryShoesRepository.save(new LexuryShoes("s1","Adidas", LexuryShoes.Type.SHOE));
            lexuryShoesRepository.save(new LexuryShoes("s2","Nike", LexuryShoes.Type.SHOE));
            lexuryShoesRepository.save(new LexuryShoes("s3","XShoe", LexuryShoes.Type.SHOE));
        };
    }
}
